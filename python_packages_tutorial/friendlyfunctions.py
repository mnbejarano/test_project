
def greetings(name):
    """Provide a friendly greeting addressed to `name`.

    Parameters
    ----------
    name : string
        This is the name that the greeting should be addressed to.
    """
    greeting = 'Greetings, ' + str(name) + '!'
    print(greeting)




